import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { GiftService } from 'src/app/core/service/gift.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  gifts = [];

  constructor(public giftService: GiftService) { }

  ngOnInit() {
    this.giftService.getTopCards()
      .subscribe(
        response => {
          console.log(response);
          this.gifts = response;
        });
  }
}
