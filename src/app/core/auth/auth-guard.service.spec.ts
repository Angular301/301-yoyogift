import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { AuthGuardService } from './auth-guard.service';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/internal/Observable';
import { of } from 'rxjs/internal/observable/of';

class MockService {
  public select(mapFn: any, ...paths: string[]): Observable<any> {
    const val = {
      globalState: {
        transactionInitState: [{}],
        accountAmountState: [{}]
      }
    };
    return of([val]);
  }
  public dispatch(): void {
    return null;
  }
}
describe('AuthGuardService', () => {
  let service: AuthGuardService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: Router, useValue: MockService },
        { provide: Store, useValue: MockService }
      ]
    });
    service = TestBed.get(AuthGuardService);
  });
  // it('can load instance', () => {
  //   expect(service).toBeTruthy();
  // });

  // it('can call canActivate', () => {
  //   service.canActivate(, '');
  // });
});
