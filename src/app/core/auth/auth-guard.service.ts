import { Injectable } from '@angular/core';
import {
  CanActivate,
  Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { Store } from '@ngrx/store';

import { State } from './authstore/state.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {
  constructor(public router: Router, private store: Store<{ authState }>) {
    //
  }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean | Observable<any> {
    return this.store.select('authState').pipe(
      map((authState: State) => {
        if (authState.isLoggedIn) {
          return true;
        } else {
          this.router.navigate(['']);
        }
      })
    );
  }
}
