import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router, CanActivate } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RoleGuardService implements CanActivate  {
  adminLoggedIn: string;

  constructor(public router: Router) {
    //
  }
  canActivate(route: ActivatedRouteSnapshot): boolean {
    this.adminLoggedIn = localStorage.getItem('isAdmin');
    if (!this.adminLoggedIn) {
      this.router.navigate(['/']);
      return false;
    }
    return true;
  }
}
