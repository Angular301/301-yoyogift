import {
    SocialLoginModule,
    AuthServiceConfig,
    GoogleLoginProvider,
    FacebookLoginProvider,
  } from "angular-6-social-login";
  
  export function getAuthServiceConfigs() {
    let config = new AuthServiceConfig(
        [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider('21297241046-i55ect17ev2g3kv17bo81eovitvq79v6.apps.googleusercontent.com')
          }
        ]
    );
    return config;
  }