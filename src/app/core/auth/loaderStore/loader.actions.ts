import { Action } from '@ngrx/store';

export const startLoader = "[loader] Start loader";
export const stopLoader = "[loader] Stop loader";

export class StartLoader implements Action {
    readonly type = startLoader;
}

export class StopLoader implements Action {
    readonly type = stopLoader;
}

export type LoaderActions = StartLoader | StopLoader;