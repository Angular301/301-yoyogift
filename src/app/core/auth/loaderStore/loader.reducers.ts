import { Store } from '@ngrx/store';

import * as LoaderActions from './loader.actions';
import { LoaderState } from './loader.state';

export const initialState: LoaderState = {
    loader: false
}
export function loaderReducers(state = initialState, action: LoaderActions.LoaderActions) {
    switch(action.type) {
        case LoaderActions.startLoader:
            return {
                loader: true
            }
        case LoaderActions.stopLoader:
            return {
                loader: false
            }
        default:
            return state;
    }
}