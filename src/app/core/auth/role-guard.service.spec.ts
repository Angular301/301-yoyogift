import { TestBed } from '@angular/core/testing';
import { RoleGuardService } from './role-guard.service';
import { Router } from '@angular/router';

class MockService {
  //
}
describe('RoleGuardService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      { provide: Router, MockService }
    ]
  }));

  it('should be created', () => {
    const service: RoleGuardService = TestBed.get(RoleGuardService);
    expect(service).toBeTruthy();
  });
});
