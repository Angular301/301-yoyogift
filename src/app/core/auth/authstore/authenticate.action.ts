import { Action } from '@ngrx/store';

export const ON_LOGIN = 'ON_LOGIN';
export const ON_LOGOUT = 'ON_LOGOUT';
export const LOGIN_START = 'LOGIN_START';
export const LOGIN_ERROR = 'LOGIN_ERROR';
export const CHECK_USER = 'CHECK_USER';
export const ADD_USER = 'ADD_USER';
export const CHECK_EMAIL = 'CHECK_EMAIL';
export const ON_HTTP_CALL = 'ON_HTTP_CALL';
export const ON_RESPONSE_CALL = 'ON_RESPONSE_CALL';

export class OnLogin implements Action {
    readonly type = ON_LOGIN;
    constructor(public payload){}
}

export class OnLogout implements Action {
    readonly type = ON_LOGOUT;
}

export class LoginStart implements Action {
    readonly type = LOGIN_START;

}

export class LoginError implements Action {
    readonly type = LOGIN_ERROR;
}

export class CheckUser implements Action {
    readonly type = CHECK_USER;
    constructor(public payload) {
    }
}

export class AddUser implements Action {
    readonly type = ADD_USER;
    constructor(public payload) {
    }
}

export class CheckEmail implements Action {
    readonly type = CHECK_EMAIL;
}

export class OnHttpCall implements Action {
    readonly type = ON_HTTP_CALL;
}

export class OnResponseCall implements Action {
    readonly type = ON_RESPONSE_CALL;
}




export type AuthActions = OnLogin | OnLogout | LoginStart | LoginError | CheckEmail | OnHttpCall | OnResponseCall;
