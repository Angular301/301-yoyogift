import * as Actions from './authenticate.action';
import { State } from './state.model';
import { User } from 'src/app/shared/model/user.model';

const initialState: State = {
    isLoggedIn: false,
    user: null,
    error: false,
    loader: false
};

export function authenticateReducers(state = initialState, action: Actions.AuthActions) {
    switch (action.type) {
        case Actions.ON_LOGIN:
            // state = {
            //     isLoggedIn: true
            // }
            console.log(action.payload);
            return {
                ...state,
                isLoggedIn: true,
                user: action.payload,
                error: false
            };
        case Actions.ON_LOGOUT:
            localStorage.removeItem('email');
            return {
                ...state,
                isLoggedIn: false,
                user: null,
                error: false
            };
        case Actions.LOGIN_ERROR:
            return {
                ...state,
                isLoggedIn: false,
                user: null,
                error: true
            };
        case Actions.ON_HTTP_CALL:
            return {
                ...state,
                loader: true
            };
        case Actions.ON_RESPONSE_CALL:
            return {
                ...state,
                loader: false
            };
        default:
            return state;
    }
}
