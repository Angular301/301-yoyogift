import { Actions, ofType, Effect } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { switchMap, catchError, map } from 'rxjs/operators';
import { AuthService, GoogleLoginProvider } from 'angular-6-social-login';

import * as AuthActions from './authenticate.action';
import { of } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AuthenticateEffects {
  @Effect()
  authLogin = this.actions$.pipe(
    ofType(AuthActions.LOGIN_START),
    switchMap(() => {
      return this.socialAuthService
        .signIn(GoogleLoginProvider.PROVIDER_ID)
        .then(data => {
          console.log(data);
          return new AuthActions.CheckUser(data);
        })
        .catch(error => {
          console.log(error);
          return new AuthActions.LoginError();
        });
    })
  );

  @Effect()
  checkUser = this.actions$.pipe(
    ofType(AuthActions.CHECK_USER),
    switchMap((authData: AuthActions.CheckUser) => {
      return this.httpClient
        .get(
          'https://json-repo.herokuapp.com/users?email=' +
            authData.payload.email
        )
        .pipe(
          map(
            (data: Array<any>) => {
              if (data.length === 0) {
                return new AuthActions.AddUser(authData.payload);
              } else {
                localStorage.setItem('email', authData.payload.email);
                return new AuthActions.OnLogin(data[0]);
              }
            },
            catchError(() => {
              return of(new AuthActions.LoginError());
            })
          )
        );
    })
  );

  @Effect()
  addUser = this.actions$.pipe(
    ofType(AuthActions.ADD_USER),
    switchMap((authData: AuthActions.AddUser) => {
      const obj = {
        name: authData.payload.name,
        email: authData.payload.email,
        balancePoints: 5000,
        isAdmin: false,
        favorites: [],
        image: authData.payload.image
      };
      return this.httpClient
        .post('https://json-repo.herokuapp.com/users', obj)
        .pipe(
          map(
            res => {
              localStorage.setItem('email', authData.payload.email);
              return new AuthActions.OnLogin(res);
            },
            catchError(() => {
              return of(new AuthActions.LoginError());
            })
          )
        );
    })
  );

  @Effect()
  checkEmail = this.actions$.pipe(
    ofType(AuthActions.CHECK_EMAIL),
    switchMap((authData: AuthActions.CheckEmail) => {
      console.log(localStorage.getItem('email'));
      return this.httpClient
        .get(
          `https://json-repo.herokuapp.com/users?email=${localStorage.getItem(
            'email'
          )}`
        )
        .pipe(
          map(
            res => {
              // return new AuthActions.OnLogin(obj);
              if (res != null) {
                return new AuthActions.OnLogin(res[0]);
              } else {
                return new AuthActions.OnLogout();
              }
            },
            catchError(() => {
              return of(new AuthActions.LoginError());
            })
          )
        );
    })
  );

  constructor(
    private actions$: Actions,
    private socialAuthService: AuthService,
    private httpClient: HttpClient
  ) {}
}
