import { User } from 'src/app/shared/model/user.model';

export interface State {
    isLoggedIn: boolean;
    user: User;
    error: boolean;
    loader: boolean;
}
