import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ProfileComponent } from './profile.component';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { Store } from '@ngrx/store';
import { HttpServiceService } from '../../service/http-service.service';
import { Router } from '@angular/router';
import { GiftService } from '../../service/gift.service';
import { MatDialog } from '@angular/material/dialog';
import { UserService } from '../../service/user.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs/internal/Observable';
import { of } from 'rxjs/internal/observable/of';

class MockService {
  public select(mapFn: any, ...paths: string[]): Observable<any> {
    const val = {
      globalState: {
        transactionInitState: [{}],
        accountAmountState: [{}]
      }
    };
    return of([val]);
  }
  public dispatch(): void {
    return null;
  }
  public get(): any {
    return of([]);
  }
  public navigate(): any {
  }
  public getGiftReviewsByUser(): any {
    return of([]);
  }
  public addReview(): any {
    return of([]);
  }
  public updateCard(): any {
    return of([]);
  }
  public open(): any {
  }
  public updateUser(): any {
    return of([]);
  }
}
describe('ProfileComponent', () => {
  let component: ProfileComponent;
  let fixture: ComponentFixture<ProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProfileComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
      providers: [
        { provide: Store, useClass: MockService },
        { provide: HttpServiceService, useClass: MockService },
        { provide: Router, useClass: MockService },
        { provide: GiftService, useClass: MockService },
        { provide: MatDialog, useClass: MockService },
        { provide: UserService, useClass: MockService },
        { provide: MatSnackBar, useClass: MockService }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
