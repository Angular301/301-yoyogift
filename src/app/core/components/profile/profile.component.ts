import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from '../../auth/authstore/state.model';
import { HttpServiceService } from '../../service/http-service.service';
import { Router } from '@angular/router';
import { EditgiftComponent } from 'src/app/shared/components/editgift/editgift.component';
import { Review } from 'src/app/shared/model/review.model';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AddGiftCardComponent } from '../add-gift-card/add-gift-card.component';
import { UserService } from '../../service/user.service';
import { GiftService } from '../../service/gift.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  user;
  sentGifts = [];
  receivedGifts = [];
  newReview: Review;
  pointsadded: any;
  TopupDialog: MatDialogRef<AddGiftCardComponent>;

  constructor(
    private store: Store<{ authState: State }>,
    private httpService: HttpServiceService,
    private router: Router,
    private giftService: GiftService,
    public dialog: MatDialog,
    public dialogModel: MatDialog,
    public userService: UserService,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.store.select('authState').subscribe(data => {
      this.user = data.user;
      this.httpService
        .get(`/user-gifts?senderId=${this.user.id}`)
        .subscribe(res => {
          this.sentGifts = res;
          console.log(this.sentGifts);
        });
      this.httpService
        .get(`/user-gifts?receiverEmail=${this.user.email}`)
        .subscribe(item => {
          this.receivedGifts = item;
          console.log(this.receivedGifts);
        });
    });
  }

  onGiftAction(gid) {
    this.router.navigate(['gifts/details', gid]);
  }

  rateAndReview(giftId: string) {
    this.giftService
      .getGiftReviewsByUser(giftId, this.user.id)
      .subscribe(review => {
        if (review.length === 0) {
          // no review so far
          const dialogRef = this.dialog.open(EditgiftComponent, {
            width: '250px',
            data: {
              gift: giftId,
              user: this.user,
              message: 'ratenow'
            }
          });
          dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
            console.log(result);
            if (result) {
              this.newReview = new Review(
                this.user.id,
                this.user.name,
                giftId,
                result.rating,
                result.review.reviewControl,
                new Date()
              );
              // console.log('New Review Object: ' + this.newReview);
              this.giftService
                .addReview(this.newReview)
                .subscribe(
                  addedReview => this.openSnackBar('Thanks for your feedback!!!'),
                  error => this.openSnackBar('There was an while submitting review!!! \n Check your internet connection and Try Again!!!')
                );
              this.giftService.getCardsById(giftId).subscribe(gift => {
                gift.rating = (gift.rating + result.rating) / 2;
                gift.rating.toFixed(1);
                this.giftService
                  .updateCard(gift)
                  .subscribe(
                    updatedCard => console.log('Card updated with new rating'),
                    error =>
                      console.log(
                        'There was an while updating card with new rating'
                      )
                  );
              });
            }
          });
        }
        if (review.length === 1) {
          // already reviewed
          console.log('came here');
          const dialogRef = this.dialog.open(EditgiftComponent, {
            width: '250px',
            data: {
              rating: review[0].rating,
              review: review[0].review,
              message: 'viewrate'
            }
          });
          dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
            console.log(result);
          });
        }
      });
  }

  topup() {
    this.TopupDialog = this.dialogModel.open(AddGiftCardComponent, {
      width: '50%',
      height: '50%',
      data: { message: 'topup' }
    });

    this.TopupDialog.afterClosed().subscribe(result => {
      if (result) {
        this.pointsadded = result.data.addpoints;
        this.user.balancePoints =
          // tslint:disable-next-line:radix
          parseInt(this.user.balancePoints) + parseInt(this.pointsadded);

        this.userService
          .updateUser(this.user)
          .subscribe(
            msg => this.openSnackBar('Top Up Success!!!'),
            error => this.openSnackBar('There was an error while Top Up!!! \n Check your internet connection and try angain!!!')
          );
      }
    });
  }

  openSnackBar(inputval: string) {
    this._snackBar.open(inputval, '', {
      duration: 3000
    });
  }
}
