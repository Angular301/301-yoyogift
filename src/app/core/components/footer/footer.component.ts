import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  public langButton: string;
  public selectedLanguage: string;

  constructor(private translate: TranslateService) {
    this.selectedLanguage = localStorage.getItem('language');
    translate.setDefaultLang(this.selectedLanguage);
  }

  public ngOnInit(): void {
    if (this.selectedLanguage === 'fr') {
      this.langButton = 'English';
    } else {
      this.langButton = 'French';
    }
  }

  switchLanguage() {
    if (this.selectedLanguage === 'en') {
      this.langButton = 'English';
      this.selectedLanguage = 'fr';
      localStorage.setItem('language', 'fr');
      this.translate.use(this.selectedLanguage);
    } else if (this.selectedLanguage === 'fr') {
      this.langButton = 'French';
      this.selectedLanguage = 'en';
      localStorage.setItem('language', 'en');
      this.translate.use(this.selectedLanguage);
    }
  }

}
