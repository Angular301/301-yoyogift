import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NavBarComponent } from './nav-bar.component';
import { NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpServiceService } from '../../service/http-service.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { of } from 'rxjs/internal/observable/of';

class MockService {
  public get(): any {
    return of(['']);
  }

  public navigate(): any {
    return;
  }
}

describe('NavBarComponent', () => {
  let component: NavBarComponent;
  let fixture: ComponentFixture<NavBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NavBarComponent],
      providers: [
        { provide: HttpServiceService, useClass: MockService },
        { provide: HttpClient, useClass: MockService },
        { provide: Router, useClass: MockService }
      ],
      schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call routeToGiftPage', () => {
    component.routeToGiftPage('');
  });

  it('should call routeToGiftPage when navLink is not empty', () => {
    component.routeToGiftPage('1');
  });
});
