import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { DataService } from '../../service/data.service';
import { HttpServiceService } from '../../service/http-service.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {
  navLinks: string[];
  activeIndex: any;
  constructor(
    public http: HttpClient,
    public router: Router,
    public dataService: DataService,
    public httpService: HttpServiceService
  ) { }
  categoryClicked = 0;
  ngOnInit() {
    this.httpService.get('/category').subscribe((data: any[]) => {
      this.navLinks = data;
    });
  }

  routeToGiftPage(navLink) {
    this.categoryClicked = navLink;
    this.dataService.setGiftCategory(navLink);
    if (navLink) {
      this.router.navigate(['/gifts'], { queryParams: { source: 'category', id: navLink } });
    }
  }

  public setActiveIndex(index: any): void {
    this.activeIndex = index;
  }

}
