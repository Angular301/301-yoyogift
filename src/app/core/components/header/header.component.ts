import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import * as AuthActions from '../../auth/authstore/authenticate.action';
import { State } from '../../auth/authstore/state.model';
import { HttpServiceService } from '../../service/http-service.service';


import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AddGiftCardComponent } from '../add-gift-card/add-gift-card.component';
import { GiftService } from '../../service/gift.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public loggedIn = false;
  public error = false;
  public name: any;
  isAdmin: any;
  public img: any;
  AddDialog: MatDialogRef<AddGiftCardComponent>;

  constructor(
    public router: Router,
    private store: Store<{ authState: State }>,
    public httpService: HttpServiceService,
    private dialogModel: MatDialog,
    private giftservice: GiftService,
    private _snackBar: MatSnackBar,
  ) { }

  ngOnInit() {
    this.store.select('authState').subscribe(data => {
      this.loggedIn = data.isLoggedIn;
      this.error = data.error;
      if (data.user !== null) {
        this.img = data.user.image;
        this.name = data.user.name;
        this.isAdmin = data.user.isAdmin;
      }
    });

    if (localStorage.getItem('email') != null) {
      this.store.dispatch(new AuthActions.CheckEmail());
    } else {
      this.store.dispatch(new AuthActions.OnLogout());
    }
  }

  public routeToMainPage(): void {
    this.router.navigate(['/']);
  }

  loginWithGoogle() {
    this.store.dispatch(new AuthActions.LoginStart());
  }

  onLogout() {
    this.isAdmin = false;
    this.router.navigate(['']);
    this.store.dispatch(new AuthActions.OnLogout());
  }

  routeToAdminDashboard() {
    this.router.navigate(['/admin']);
  }
  gotoProfile() {
    this.router.navigate(['profile']);
  }

  AddCard() {

    this.AddDialog = this.dialogModel.open(AddGiftCardComponent, {
      disableClose: true,
      width: '50%',
      height: '80%',
      data: { message: 'addcard' }
    });
  }

}
