import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MatSnackBar } from '@angular/material/snack-bar';
import { GiftService } from '../../service/gift.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { HttpServiceService } from '../../service/http-service.service';

@Component({
  selector: 'app-add-gift-card',
  templateUrl: './add-gift-card.component.html',
  styleUrls: ['./add-gift-card.component.scss']
})
export class AddGiftCardComponent implements OnInit {
  AddForm: FormGroup;
  TopupForm: FormGroup;
  category: any;

  constructor(private giftservice: GiftService, private _snackBar: MatSnackBar, private httpService: HttpServiceService,
    public dialogRef: MatDialogRef<AddGiftCardComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.httpService.get('/category').subscribe(
      response => {
        this.category = response;
      });

    this.AddForm = new FormGroup({
      name: new FormControl('', Validators.required),
      brand: new FormControl('', Validators.required),
      desc: new FormControl(''),
      imageUrl: new FormControl('', Validators.required),
      buyoutPoints: new FormControl('', [Validators.required, Validators.maxLength(5), Validators.pattern(/^.?(0|[1-9]\d*)?$/)]),
      categoryId: new FormControl('', Validators.required),
      rating: new FormControl('', [Validators.required, Validators.maxLength(1), Validators.pattern(/^.?(0|[1-5]\d*)?$/)]),
      discounts: new FormControl('', [Validators.required, Validators.maxLength(2), Validators.pattern(/^%?(0|[1-9]\d*)?$/)])
    });

    this.TopupForm = new FormGroup({
      addpoints: new FormControl('', [Validators.required, Validators.maxLength(5), Validators.pattern(/^.?(0|[1-9]\d*)?$/)])
    });

    if (this.data.message === 'addcard') {
      this.dialogRef.updateSize('50%', '70%');
    }
    if (this.data.message === 'topup') {
      this.dialogRef.updateSize('250px', '250px');
    }

  }

  public close(): void {
    this.dialogRef.close();
  }

  savecard(result) {
    console.log(result);
    this.giftservice.addCard(result)
      .subscribe(
        Gift => this.openSnackBaredit('Saved successfully!'),
        (error) => this.openSnackBaredit('could not save')
      );
  }

  openSnackBaredit(inputval) {
    const snackBaredit = this._snackBar.open(inputval, '', {
      duration: 3000
    });
  }

  AddPoints() {
    this.dialogRef.close({ data: this.TopupForm.value });
  }

}
