import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AddGiftCardComponent } from './add-gift-card.component';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpServiceService } from '../../service/http-service.service';
import { of } from 'rxjs';
import { GiftService } from '../../service/gift.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MaterialModule } from 'src/app/shared/material.module';

class MockService {
  public get(): any {
    return of([]);
  }

  public addCard(): any {
    return of([]);
  }

  public open(): any { }
  public close(): any { }
}

describe('AddGiftCardComponent', () => {
  let component: AddGiftCardComponent;
  let fixture: ComponentFixture<AddGiftCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AddGiftCardComponent],
      imports: [MaterialModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
      providers: [
        { provide: HttpServiceService, useClass: MockService },
        { provide: GiftService, useClass: MockService },
        { provide: MatSnackBar, useClass: MockService },
        { provide: MatDialogRef, useClass: MockService },
        { provide: MAT_DIALOG_DATA, useClass: MockService },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddGiftCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should call close', () => {
    component.close();
  });
  it('should call openSnackBaredit', () => {
    component.openSnackBaredit('');
  });
  it('should call AddPoints', () => {
    component.AddPoints();
  });
  it('should call savecard', () => {
    component.savecard('');
  });
});
