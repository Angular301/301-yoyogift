import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { LoaderState } from '../../auth/loaderStore/loader.state';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
  loader = false;
  constructor(private store: Store<{loaderState: LoaderState}>) { }

  ngOnInit() {
    this.store.select('loaderState').subscribe(
      data => {
        this.loader = data.loader;
        console.log(data);
      }
    )
  }

  public routetoTop(): void {
    window.scroll(0, 0);
  }

}
