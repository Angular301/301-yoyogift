import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpResponse, HttpEvent } from '@angular/common/http';
import { State } from '../auth/authstore/state.model';
import { Store } from '@ngrx/store';

import * as LoaderActions from '../auth/loaderStore/loader.actions';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { LoaderState } from '../auth/loaderStore/loader.state';

@Injectable({
  providedIn: 'root'
})
export class InterceptorsService implements HttpInterceptor {

  constructor(private store: Store<{loaderState: LoaderState}>) { }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>{
    console.log('interceptor')
    this.store.dispatch(new LoaderActions.StartLoader);
    return next.handle(req)
    .pipe(tap((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
        this.store.dispatch(new LoaderActions.StopLoader);
      }
    },
      (err: any) => {
        this.store.dispatch(new LoaderActions.StopLoader);
      }
    ));
  }
}
