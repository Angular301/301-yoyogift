import { TestBed } from '@angular/core/testing';
import { InterceptorsService } from './interceptors.service';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/internal/Observable';
import { of } from 'rxjs/internal/observable/of';

class MockService {
  public select(mapFn: any, ...paths: string[]): Observable<any> {
    const val = {
      globalState: {
        transactionInitState: [{}],
        accountAmountState: [{}]
      }
    };
    return of([val]);
  }
  public dispatch(): void {
    return null;
  }
}
describe('InterceptorsService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      { provide: Store, useClass: MockService }
    ]
  }));

  it('should be created', () => {
    const service: InterceptorsService = TestBed.get(InterceptorsService);
    expect(service).toBeTruthy();
  });
});
