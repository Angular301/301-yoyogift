import { TestBed } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import { Gift } from 'src/app/shared/model/gift.model';
import { GiftService } from './gift.service';
describe('GiftService', () => {
  let service: GiftService;
  beforeEach(() => {
    const httpClientStub = {
      get: jSON_REPO_URL_GIFT1 => ({}),
      post: (jSON_REPO_URL_GIFT1, newGift2) => ({}),
      put: (arg1, newCard2) => ({}),
      delete: arg1 => ({})
    };
    const giftStub = { id: {} };
    TestBed.configureTestingModule({
      providers: [
        { provide: HttpClient, useValue: httpClientStub },
        { provide: Gift, useValue: giftStub }
      ]
    });
    service = TestBed.get(GiftService);
  });
  it('can load instance', () => {
    expect(service).toBeTruthy();
  });
  describe('addCard', () => {
    it('makes expected calls', () => {
      const httpClientStub: HttpClient = TestBed.get(HttpClient);
      const giftStub: Gift = TestBed.get(Gift);
      spyOn(httpClientStub, 'post').and.callThrough();
      service.addCard(giftStub);
      expect(httpClientStub.post).toHaveBeenCalled();
    });
  });
  describe('updateCard', () => {
    it('makes expected calls', () => {
      const httpClientStub: HttpClient = TestBed.get(HttpClient);
      spyOn(httpClientStub, 'put').and.callThrough();
      service.updateCard({
        name: '',
        brand: '',
        desc: '',
        imageUrl: '',
        buyoutPoints: 0,
        expiryDays: 0,
        categoryId: 0,
        rating: 0,
        discount: 0,
        id: ''
      });
      expect(httpClientStub.put).toHaveBeenCalled();
    });
  });
  describe('getAllCards', () => {
    it('makes expected calls', () => {
      const httpClientStub: HttpClient = TestBed.get(HttpClient);
      spyOn(httpClientStub, 'get').and.callThrough();
      service.getAllCards();
      expect(httpClientStub.get).toHaveBeenCalled();
    });
  });
  describe('getTopCards', () => {
    it('makes expected calls', () => {
      const httpClientStub: HttpClient = TestBed.get(HttpClient);
      spyOn(httpClientStub, 'get').and.callThrough();
      service.getTopCards();
      expect(httpClientStub.get).toHaveBeenCalled();
    });
  });
  describe('getCardsById', () => {
    it('makes expected calls', () => {
      const httpClientStub: HttpClient = TestBed.get(HttpClient);
      spyOn(httpClientStub, 'get').and.callThrough();
      service.getCardsById('1');
      expect(httpClientStub.get).toHaveBeenCalled();
    });
  });
  describe('deleteCard', () => {
    it('makes expected calls', () => {
      const httpClientStub: HttpClient = TestBed.get(HttpClient);
      spyOn(httpClientStub, 'get').and.callThrough();
      service.deleteCard('1');
    });
  });
  describe('getGiftReviews', () => {
    it('makes expected calls', () => {
      const httpClientStub: HttpClient = TestBed.get(HttpClient);
      spyOn(httpClientStub, 'get').and.callThrough();
      service.getGiftReviews('1');
      expect(httpClientStub.get).toHaveBeenCalled();
    });
  });
});
