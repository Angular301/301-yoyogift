import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable, pipe } from 'rxjs';
import { map, catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HttpServiceService {

  constructor(
    public http: HttpClient
  ) { }

  public get(endpoint: string): Observable<any> {
    return this.http.get(environment.baseUrl + endpoint).pipe(map(res => res));
  }

  public getCategoryGifts(id): any {
    return this.http.get(`${environment.baseUrl}/gifts?categoryId=${id}`);
  }

  public getAllGifts(start, end) {
    return this.http.get(`${environment.baseUrl}/gifts?_start=${start}&_end=${end}`);
  }
}
