import { TestBed } from '@angular/core/testing';
import { HttpServiceService } from './http-service.service';
import { HttpClient } from '@angular/common/http';
class MockService { }
describe('HttpServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      { provide: HttpClient, useClass: MockService }
    ]
  }));

  it('should be created', () => {
    const service: HttpServiceService = TestBed.get(HttpServiceService);
    expect(service).toBeTruthy();
  });
});
