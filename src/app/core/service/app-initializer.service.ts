import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from '../auth/authstore/state.model';
import * as AuthActions from './../auth/authstore/authenticate.action';

@Injectable({
  providedIn: 'root'
})
export class AppInitializerService {

  constructor(
    private store: Store<{authState: State}>
  ) { }

  Init() {
    return new Promise<void>((resolve, reject) => {

      if (localStorage.getItem('language') === null) {
        localStorage.setItem('language', 'en');
      }
      if (localStorage.getItem('email') !== null) {
        this.store.dispatch(new AuthActions.CheckEmail);
      }
      resolve();
    });
  }
}
