import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Gift } from 'src/app/shared/model/gift.model';
import { Review } from 'src/app/shared/model/review.model';
import { UserGift } from 'src/app/shared/model/user-gift.class';

const JSON_REPO_URL_GIFT = 'https://json-repo.herokuapp.com/gifts/';
const JSON_REPO_URL_REVIEW = 'https://json-repo.herokuapp.com/comments-review/';
const JSON_REPO_URL_USERGIFT = 'https://json-repo.herokuapp.com/user-gifts/';

@Injectable({
  providedIn: 'root'
})
export class GiftService {
  constructor(public http: HttpClient) {}

  getAllCards(): Observable<Gift[]> {
    return this.http.get<Gift[]>(JSON_REPO_URL_GIFT);
  }

  getTopCards(): Observable<Gift[]> {
    return this.http.get<Gift[]>(JSON_REPO_URL_GIFT + '?categoryId=1&_limit=6');
  }

  getCardsById(id: string): Observable<Gift> {
    return this.http.get<Gift>(JSON_REPO_URL_GIFT + id);
  }

  addCard(newGift: Gift): Observable<Gift> {
    return this.http.post<Gift>(JSON_REPO_URL_GIFT, newGift);
  }

  updateCard(newCard: Gift): Observable<Gift> {
    return this.http.put<Gift>(JSON_REPO_URL_GIFT + newCard.id, newCard);
  }

  deleteCard(id: string): Observable<Gift> {
    return this.http.delete<Gift>(JSON_REPO_URL_GIFT + id);
  }

  // TODO: Need to move to review service
  getGiftReviews(id: string): Observable<Gift[]> {
    return this.http.get<Gift[]>(JSON_REPO_URL_GIFT + id + '/comments-review');
  }

  getGiftReviewsByUser(giftId: string, userId: string): Observable<Review[]> {
    return this.http.get<Review[]>(JSON_REPO_URL_GIFT + giftId + '/comments-review?userId=' + userId);
  }

  addReview(review: Review): Observable<Review> {
    return this.http.post<Review>(JSON_REPO_URL_REVIEW, review);
  }

  addUserGift(userGift: UserGift): Observable<UserGift> {
    return this.http.post<UserGift>(JSON_REPO_URL_USERGIFT, userGift);
  }
}
