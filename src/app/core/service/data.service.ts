import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  public GiftCatergory = new BehaviorSubject(null);
  constructor() { }

  public getGiftCategory(): Observable<string> {
    return this.GiftCatergory.asObservable();
  }

  public setGiftCategory(value): void {
    this.GiftCatergory.next(value);
  }
}
