import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from 'src/app/shared/model/user.model';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { State } from '../auth/authstore/state.model';
import { map } from 'rxjs/internal/operators/map';

const JSON_REPO_URL_USER = 'https://json-repo.herokuapp.com/users';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(public http: HttpClient, private store: Store<{ authState }>) {}
  getAllUsers(): Observable<User[]> {
    return this.http.get<User[]>(JSON_REPO_URL_USER);
  }

  getUserById(id: string): Observable<User> {
    return this.http.get<User>(JSON_REPO_URL_USER + '/' + id);
  }

  getUserByEmail(email: string): Observable<User[]> {
    return this.http.get<User[]>(JSON_REPO_URL_USER + '?email=' + email);
  }

  addUser(newUser: User): Observable<User> {
    return this.http.post<User>(JSON_REPO_URL_USER, newUser);
  }

  updateUser(newUser: User): Observable<User> {
    console.log(newUser);
    return this.http.put<User>(JSON_REPO_URL_USER + '/' + newUser.id, newUser);
  }

  deleteUser(id: string): Observable<User> {
    return this.http.delete<User>(JSON_REPO_URL_USER + '/' + id);
  }
}
