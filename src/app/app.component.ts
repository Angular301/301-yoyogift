import { Component, OnInit } from '@angular/core';
import { HttpServiceService } from './core/service/http-service.service';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import * as UserActions from './user/userStore/user.actions';
import { SwUpdate } from '@angular/service-worker';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'yoyogift';
  obse: Observable<any>;
  constructor(
    public swUpdate: SwUpdate,
    private httpService: HttpServiceService,
    private store: Store<{ userState }>,
    private authStore: Store<{ authState }>
  ) {
    swUpdate.available.subscribe(event => {
      swUpdate.activateUpdate().then(() => document.location.reload());
    });
  }
  ngOnInit() {
    this.store.select('userState').subscribe(
      data => { }
    );

    this.getAllGifts(0, 500);
  }
  getAllGifts(start: any, end: any) {
    this.httpService.getAllGifts(start, end).subscribe((data: Array<any>) => {
      if (data.length < 100) {
        this.store.dispatch(new UserActions.AddGifts(data));
        return;
      } else {
        this.store.dispatch(new UserActions.AddGifts(data));
        this.getAllGifts(end, end + 500);
      }
    });
  }
}
