import { Component, OnInit } from '@angular/core';
import { UserDataService } from '../services/user-data.service';

export interface UserStats {
  name: string;
  email: string;
  balancePoints: number;
  id: number;
}

@Component({
  selector: 'app-manage-users',
  templateUrl: './manage-users.component.html',
  styleUrls: ['./manage-users.component.scss']
})
export class ManageUsersComponent implements OnInit {

  displayedColumn1: string[] = ['id', 'name', 'email', 'balancePoints'];
  displayedColumn2: string[] = ['id', 'name', 'email'];
  allUserData: UserStats[] = [];
  allAdminData: UserStats[] = [];

  constructor(
    public userdataService: UserDataService
  ) { }

  ngOnInit() {
    this.userdataService.getAllUsers().subscribe(users => {
      this.allUserData = users.filter(item => !item.isAdmin).map(user => {
        return {
          id: user.id,
          name: user.name,
          email: user.email,
          balancePoints: user.balancePoints
        };
      });
      this.allAdminData = users.filter(item => item.isAdmin).map(user => {
        return {
          id: user.id,
          name: user.name,
          email: user.email
        };
      });
    });
  }
}
