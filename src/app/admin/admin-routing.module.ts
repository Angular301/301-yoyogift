import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { AllGiftsComponent } from './all-gifts/all-gifts.component';
import { RoleGuardService } from '../core/auth/role-guard.service';
import { ManageUsersComponent } from './manage-users/manage-users.component';

const routes: Routes = [
  {
    path: '',
    component: AdminDashboardComponent
    // canActivate: [RoleGuardService]
  },
  {
    path: 'allgifts',
    component: AllGiftsComponent
    // canActivate: [RoleGuardService]
  },
  {
    path: 'userstats',
    component: ManageUsersComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class AdminRoutingModule { }
