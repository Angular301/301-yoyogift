import { Component, OnInit } from '@angular/core';
import * as Chart from 'chart.js';
import { ChartDataService } from '../services/chart-data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.scss']
})
export class AdminDashboardComponent implements OnInit {
  canvas: any;
  ctx: any;
  public allcategoryCount = [];
  constructor(
    public chartDataService: ChartDataService,
    public router: Router
  ) { }

  ngOnInit() {
    this.chartDataService.getAllCategory().subscribe((category) => {
      category.forEach(element => {
        this.allcategoryCount.push(element.count);
      });
      this.canvas = document.getElementById('myChart');
      this.ctx = this.canvas.getContext('2d');
      // tslint:disable-next-line:no-unused-expression
      new Chart(this.ctx, {
        type: 'pie',
        data: {
          labels: ['Apparel', 'Entertainment', 'Fashion', 'Food', 'Furniture', 'Health', 'Travel'],
          datasets: [{
            label: '# of Votes',
            data: this.allcategoryCount,
            backgroundColor: [
              'rgba(255, 99, 132, 1)',
              'rgba(54, 162, 235, 1)',
              'rgba(255, 206, 86, 1)',
              'rgba(103, 0, 19, 1)',
              'rgba(0, 89, 78, 1)',
              'rgba(61, 18, 52, 1)',
              'rgba(89, 90, 17, 1)'
            ],
            borderWidth: 1
          }]
        }
      });
    });
  }

}
