import { TestBed } from '@angular/core/testing';

import { ChartDataService } from './chart-data.service';
import { HttpClient } from '@angular/common/http';

class MockService { }

describe('ChartDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      { provide: HttpClient, useClass: MockService }
    ]
  }));

  it('should be created', () => {
    const service: ChartDataService = TestBed.get(ChartDataService);
    expect(service).toBeTruthy();
  });
});
