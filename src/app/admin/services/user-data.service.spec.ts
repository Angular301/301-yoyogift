import { TestBed } from '@angular/core/testing';

import { UserDataService } from './user-data.service';
import { HttpClient } from '@angular/common/http';

class MockService { }

describe('UserDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      { provide: HttpClient, useClass: MockService }
    ]
  }));

  it('should be created', () => {
    const service: UserDataService = TestBed.get(UserDataService);
    expect(service).toBeTruthy();
  });
});
