import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/internal/operators/map';
import { environment } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class ChartDataService {

  public categoryData;
  constructor(
    public http: HttpClient
  ) { }

  public getAllCategory(): Observable<any> {
    return this.http.get(environment.baseUrl + '/category');
  }
}
