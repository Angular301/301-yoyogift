import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class UserDataService {

  constructor(
    public http: HttpClient
  ) { }

  public getAllUsers(): Observable<any> {
    return this.http.get(environment.baseUrl + '/users');
  }
}
