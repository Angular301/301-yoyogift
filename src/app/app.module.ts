import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { authenticateReducers } from './core/auth/authstore/authenticate.reducers';
import { AuthenticateEffects } from './core/auth/authstore/authenticate.effects';
import { HttpServiceService } from './core/service/http-service.service';
import { userStoreReducers } from './user/userStore/user.reducers';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AppInitializerService } from './core/service/app-initializer.service';
import { ServiceWorkerModule } from '@angular/service-worker';

import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { environment } from '../environments/environment';
import { AddGiftCardComponent } from './core/components/add-gift-card/add-gift-card.component';
import { InterceptorsService } from './core/interceptors/interceptors.service';
import { loaderReducers } from './core/auth/loaderStore/loader.reducers';
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, '../assets/i18n/', '.json');
}

export function initializeApp(appInitService: AppInitializerService) {
  return (): Promise<any> => {
    return appInitService.Init();
  };
}

@NgModule({
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    CoreModule,
    DashboardModule,
    RouterModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    StoreModule.forRoot({
      authState: authenticateReducers,
      userState: userStoreReducers,
      loaderState: loaderReducers
    }),
    EffectsModule.forRoot([AuthenticateEffects]),
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  declarations: [
    AppComponent
  ],
  providers: [
    HttpServiceService,
    AppInitializerService,
    { provide: APP_INITIALIZER, useFactory: initializeApp, deps: [AppInitializerService], multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: InterceptorsService, multi: true }
  ],
  bootstrap: [AppComponent],
  entryComponents: [AddGiftCardComponent]
})
export class AppModule { }
