import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserGiftHistoryComponent } from './user-gift-history.component';

describe('UserGiftHistoryComponent', () => {
  let component: UserGiftHistoryComponent;
  let fixture: ComponentFixture<UserGiftHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserGiftHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserGiftHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
