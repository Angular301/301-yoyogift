import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserSendGiftComponent } from './user-send-gift.component';

describe('UserSendGiftComponent', () => {
  let component: UserSendGiftComponent;
  let fixture: ComponentFixture<UserSendGiftComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserSendGiftComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserSendGiftComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
