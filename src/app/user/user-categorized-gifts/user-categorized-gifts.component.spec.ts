import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { UserCategorizedGiftsComponent } from './user-categorized-gifts.component';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { HttpServiceService } from 'src/app/core/service/http-service.service';
import { DataService } from 'src/app/core/service/data.service';
import { of } from 'rxjs/internal/observable/of';
import { Observable } from 'rxjs/internal/Observable';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { Gift } from 'src/app/shared/model/gift.model';

class MockService {
  public queryParams = new BehaviorSubject({
    source: 'search',
    id: 1
  });
  public get(): any {
    return of([{}, {}]);
  }

  public select(mapFn: any, ...paths: string[]): Observable<any> {
    const val = {
      globalState: {
        transactionInitState: [{}],
        accountAmountState: [{}]
      }
    };
    return of([val]);
  }
  public dispatch(): void {
    return null;
  }

}
describe('UserCategorizedGiftsComponent', () => {
  let component: UserCategorizedGiftsComponent;
  let fixture: ComponentFixture<UserCategorizedGiftsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [],
      declarations: [UserCategorizedGiftsComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
      providers: [
        { provide: Router, useClass: MockService },
        { provide: Store, useClass: MockService },
        { provide: ActivatedRoute, useClass: MockService },
        { provide: DataService, useClass: MockService },
        { provide: HttpServiceService, useClass: MockService }
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserCategorizedGiftsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.filterPrice = { name: '500-1000', valid: false, value: { from: 500, to: 1000 }, checked: false };
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call ngOnInit', () => {
    const mservice = TestBed.get(ActivatedRoute);
    spyOnProperty(mservice.queryParams, 'value', 'get').and.returnValue({
      source: 'category',
      id: 1
    });
    component.ngOnInit();
  });

  it('should call checkBrand', () => {
    component.checkBrand(1);
  });

  it('should call checkPrice', () => {
    component.checkPrice(component.filterPrice);
  });
});
