import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { DataService } from 'src/app/core/service/data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpServiceService } from 'src/app/core/service/http-service.service';
import { Store } from '@ngrx/store';
import { Gift } from 'src/app/shared/model/gift.model';
import { FormArray, FormControl } from '@angular/forms';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

interface price {
  name: string;
  valid: boolean;
  value: {
    from: number;
    to: number;
  };
  checked: boolean;
};

@Component({
  selector: 'app-user-categorized-gifts',
  templateUrl: './user-categorized-gifts.component.html',
  styleUrls: ['./user-categorized-gifts.component.scss']
})
export class UserCategorizedGiftsComponent implements OnInit {
  giftCategory: string;
  filterBrand = [];
  filterPrice: price;
  gifts = [];
  cid: number;
  isSearch = false;
  offsetFrom = 0;
  offsetTo = 20;
  price = [
    { name: 'All', valid: true, value: { from: 0, to: 0 }, checked: true },
    { name: '500-1000', valid: false, value: { from: 500, to: 1000 }, checked: false },
    { name: '1000-3000', valid: false, value: { from: 1000, to: 3000 }, checked: false },
    { name: '3000-10000', valid: false, value: { from: 3000, to: 10000 }, checked: false },
    { name: '10000-50000', valid: false, value: { from: 10000, to: 50000 }, checked: false },
    { name: '50000+', valid: false, value: { from: 500000, to: 1000000 }, checked: false }
  ];
  brands = [];
  sortOrigin = [{name: 'default', checked: true}, {name: 'A to Z', checked: false}, {name: 'Z to A', checked: false}, {name: 'Price hight to low', checked: false}, {name: 'Price hight to low', checked: false}];
  formControl = new FormControl('A to Z');
  sort = [...this.sortOrigin];
  recentUrl = '';
  constructor(
    public dataService: DataService,
    public router: Router,
    private activatedRoute: ActivatedRoute,
    private httpService: HttpServiceService,
    private store: Store<{ userState }>
  ) { }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(
      params => {
        this.offsetFrom = 0;
        this.offsetTo = 20;
        this.brands = [];
        for (let p of this.price) {
          // if(p.name === 'All') continue;
          p.valid = false;
        }
        if (params.source === 'category') {
          console.log('called');
          this.isSearch = false;
          this.sort = null;
          
          this.cid = params.id;
          console.log(this.cid);
          this.httpService.get(`/gifts?categoryId=${params.id}&_start=${this.offsetFrom}&_end=${this.offsetTo}`)
            .subscribe(
              response => {
                this.recentUrl = `/gifts?categoryId=${params.id}`;
                this.offsetFrom += 20;
                this.offsetTo += 20;
                console.log(response);
                this.gifts = response;
                this.store.select('userState').subscribe(
                  (data: { gifts: Gift[] }) => {
                    console.log('called store');
                    for (let gift of data.gifts) {
                      if (gift.categoryId == this.cid && !this.brands.includes(gift.brand)) {
                        this.brands.push(gift.brand);
                        console.log(this.brands);
                      }
                      if (gift.categoryId == this.cid) {
                        if (gift.buyoutPoints >= 500 && gift.buyoutPoints <= 1000) {
                          this.price[0].valid = true;
                        } else if (gift.buyoutPoints > 1000 && gift.buyoutPoints <= 3000) {
                          this.price[1].valid = true;;
                        } else if (gift.buyoutPoints > 3000 && gift.buyoutPoints <= 10000) {
                          this.price[2].valid = true;
                        } else if (gift.buyoutPoints > 10000 && gift.buyoutPoints <= 50000) {
                          this.price[3].valid = true;
                        } else if (gift.buyoutPoints > 50000) {
                          this.price[4].valid = true;
                        }
                      }
                    }
                  });
              });
              // this.sort = this.sortOrigin;
        } else if (params.source == 'search' && params.query != '') {
          this.isSearch = true;
          this.offsetFrom = 0;
          this.offsetTo = 20;
          this.httpService.get(`/gifts?q=${params.query}&_start=${this.offsetFrom}&_end=${this.offsetTo}`)
            .subscribe(
              response => {
                this.recentUrl = `/gifts?q=${params.query}`;
                this.offsetFrom += 20;
                this.offsetTo += 20;
                console.log(response);
                this.gifts = response;
              }
            );
        }
      }
    );
  }

  
  checkBrand(b) {
    this.offsetFrom = 0;
    this.offsetTo = 20;
    if (this.filterBrand.includes(b)) {
      this.filterBrand.splice(this.filterBrand.indexOf(b), 1)
      console.log(this.filterBrand);
    } else {
      this.filterBrand.push(b);
      console.log(this.filterBrand);
    }

    this.searchFilters();
  }

  checkPrice(p) {
    console.log(p);
    this.offsetFrom = 0;
    this.offsetTo = 20;
    if (p.name == 'All') {
      console.log('all');
      this.filterPrice = undefined;
      const str = this.getBrandsFilterUrl();
      if (str == '') {
        this.httpService.get(`/gifts?categoryId=${this.cid}&_start=${this.offsetFrom}&_end=${this.offsetTo}`)
          .subscribe(
            data => {
              this.recentUrl = `/gifts?categoryId=${this.cid}`;
              this.offsetFrom += 20;
              this.offsetTo += 20;
              this.gifts = data;
            }
          )
      } else {
        this.httpService.get(`/gifts?categoryId=${this.cid}&${str}&_start=${this.offsetFrom}&_end=${this.offsetTo}`)
          .subscribe(
            data => {
              this.recentUrl = `/gifts?categoryId=${this.cid}&${str}`;
              this.offsetFrom += 20;
              this.offsetTo += 20;
              this.gifts = data;
            }
          )
      }
    } else {
      this.filterPrice = p;

      this.searchFilters();
    }

  }

  getBrandsFilterUrl() {
    let str = '';
    for (let i = 0; i < this.filterBrand.length; i++) {

      if (i === this.filterBrand.length - 1) {
        str += `brand=${this.filterBrand[i]}`;
      } else {
        str += `brand=${this.filterBrand[i]}&`;
      }
    }
    return str;
  }

  searchFilters() {

    let str = this.getBrandsFilterUrl();
    console.log(this.filterPrice);
    if (this.filterPrice == undefined) {
      if (str != '') {
        this.httpService.get(`/gifts?categoryId=${this.cid}&${str}&_start=${this.offsetFrom}&_end=${this.offsetTo}`)
          .subscribe(
            data => {
              this.recentUrl = `/gifts?categoryId=${this.cid}&${str}`;
              this.offsetFrom += 20;
              this.offsetTo += 20;
              console.log(data);
              this.gifts = data;
            }
          )
      }
      else {
        this.httpService.get(`/gifts?categoryId=${this.cid}&_start=${this.offsetFrom}&_end=${this.offsetTo}`)
          .subscribe(
            data => {
              this.recentUrl = `/gifts?categoryId=${this.cid}`;
              this.offsetFrom += 20;
              this.offsetTo += 20;
              console.log(data);
              this.gifts = data;
            }
          )
      }
    } else {
      if (str !== '') {
        this.httpService.get(`/gifts?categoryId=${this.cid}&${str}&buyoutPoints_gte=${this.filterPrice.value.from}&buyoutPoints_lte=${this.filterPrice.value.to}&_start=${this.offsetFrom}&_end=${this.offsetTo}`)
          .subscribe(
            data => {
              this.recentUrl = `/gifts?categoryId=${this.cid}&${str}&buyoutPoints_gte=${this.filterPrice.value.from}&buyoutPoints_lte=${this.filterPrice.value.to}`;
              this.offsetFrom += 20;
              this.offsetTo += 20;
              console.log(data);
              this.gifts = data;
            }
          )
      }
      else {
        this.httpService.get(`/gifts?categoryId=${this.cid}&buyoutPoints_gte=${this.filterPrice.value.from}&buyoutPoints_lte=${this.filterPrice.value.to}&_start=${this.offsetFrom}&_end=${this.offsetTo}`)
          .subscribe(
            data => {
              this.recentUrl = `/gifts?categoryId=${this.cid}&buyoutPoints_gte=${this.filterPrice.value.from}&buyoutPoints_lte=${this.filterPrice.value.to}`;
              this.offsetFrom += 20;
              this.offsetTo += 20;
              console.log(data);
              this.gifts = data;
            }
          )
      }

    }

  }

  sortGifts(value) {
    this.offsetFrom = 0;
    this.offsetTo = 20;
    let str = this.getBrandsFilterUrl();
    if(this.filterPrice != undefined){
      if(str == '') {
        this.httpService.get(`/gifts?categoryId=${this.cid}&buyoutPoints_gte=${this.filterPrice.value.from}&buyoutPoints_lte=${this.filterPrice.value.to}&${value}&_start=${this.offsetFrom}&_end=${this.offsetTo}`)
        .subscribe(data=>{
          this.recentUrl = `/gifts?categoryId=${this.cid}&buyoutPoints_gte=${this.filterPrice.value.from}&buyoutPoints_lte=${this.filterPrice.value.to}&${value}`;
          this.offsetFrom += 20;
          this.offsetTo += 20;
          this.gifts = data;
        })
      }else {
        this.httpService.get(`/gifts?categoryId=${this.cid}&${str}&buyoutPoints_gte=${this.filterPrice.value.from}&buyoutPoints_lte=${this.filterPrice.value.to}&${value}&_start=${this.offsetFrom}&_end=${this.offsetTo}`)
        .subscribe(data=>{
          this.recentUrl = `/gifts?categoryId=${this.cid}&${str}&buyoutPoints_gte=${this.filterPrice.value.from}&buyoutPoints_lte=${this.filterPrice.value.to}&${value}`;
          this.offsetFrom += 20;
          this.offsetTo += 20;
          this.gifts = data;
        })
      }
    }else {
      if(str == '') {
        this.httpService.get(`/gifts?categoryId=${this.cid}&${value}&_start=${this.offsetFrom}&_end=${this.offsetTo}`)
        .subscribe(data=>{
          this.recentUrl = `/gifts?categoryId=${this.cid}&${value}`;
          this.offsetFrom += 20;
          this.offsetTo += 20;
          this.gifts = data;
        })
      }else {
        this.httpService.get(`/gifts?categoryId=${this.cid}&${str}&${value}&_start=${this.offsetFrom}&_end=${this.offsetTo}`)
        .subscribe(data=>{
          this.recentUrl = `/gifts?categoryId=${this.cid}&${str}&${value}`;
          this.offsetFrom += 20;
          this.offsetTo += 20;
          this.gifts = data;
        })
      }
    }
  }

  atoz(){
    this.sortGifts('_sort=name&_order=asc')
  }
  ztoa(){
    this.sortGifts('_sort=name&_order=desc')
  }
  priceHigh(){
    this.sortGifts('_sort=buyoutPoints&_order=desc')
  }
  priceLow(){
    this.sortGifts('_sort=buyoutPoints&_order=asc')
  }

  loadMore(){
    this.httpService.get(`${this.recentUrl}&_start=${this.offsetFrom}&_end=${this.offsetTo}`)
    .subscribe(
      data => {
        // this.gifts.push(data);
        this.gifts = [...this.gifts, ...data];
        this.offsetFrom += 20;
        this.offsetTo += 20;
      }
    )
  }
}
