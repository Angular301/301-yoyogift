import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserTopupComponent } from './user-topup.component';

describe('UserTopupComponent', () => {
  let component: UserTopupComponent;
  let fixture: ComponentFixture<UserTopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserTopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserTopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
