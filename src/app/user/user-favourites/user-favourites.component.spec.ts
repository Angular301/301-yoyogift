import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { UserFavouritesComponent } from './user-favourites.component';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { NavBarComponent } from 'src/app/core/components/nav-bar/nav-bar.component';
import { UserService } from 'src/app/core/service/user.service';
import { GiftService } from 'src/app/core/service/gift.service';
import { of, Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';

class MockService {
  public getUserByEmail(): any {
    return of([]);
  }
  public getCardsById(): any {
    return of([]);
  }
  public select(mapFn: any, ...paths: string[]): Observable<any> {
    const val = {
      globalState: {
        transactionInitState: [{}],
        accountAmountState: [{}]
      }
    };
    return of([val]);
  }
  public dispatch(): void {
    return null;
  }
  public open(): void {
  }
}
describe('UserFavouritesComponent', () => {
  let component: UserFavouritesComponent;
  let fixture: ComponentFixture<UserFavouritesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UserFavouritesComponent, NavBarComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
      providers: [
        { provide: UserService, useClass: MockService },
        { provide: GiftService, useClass: MockService },
        { provide: Store, useClass: MockService },
        { provide: MatSnackBar, useClass: MockService },
        { provide: MatDialog, useClass: MockService }

      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserFavouritesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should call openDialogForGiftNow', () => {
    const currentGift = {
      id: '1937374548',
      name: 'Olive - Spread Tapenade',
      brand: 'thomcook',
      desc: 'Carrier of infections with a predominantly sexual mode of transmission',
      imageUrl: 'http://dummyimage.com/500x334.png/dddddd/000000&text=Olive - Spread Tapenade',
      buyoutPoints: 503386,
      expiryDays: 78,
      categoryId: 7,
      rating: 4,
      discount: 12
    };
    component.openDialogForGiftNow(currentGift);
  });
});
