import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/core/service/user.service';
import { GiftService } from 'src/app/core/service/gift.service';
import { Store } from '@ngrx/store';
import { State } from 'src/app/core/auth/authstore/state.model';
import { User } from 'src/app/shared/model/user.model';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { Gift } from 'src/app/shared/model/gift.model';
import { EditgiftComponent } from 'src/app/shared/components/editgift/editgift.component';
import * as Email from 'emailjs-com';

@Component({
  selector: 'app-user-favourites',
  templateUrl: './user-favourites.component.html',
  styleUrls: ['./user-favourites.component.scss']
})
export class UserFavouritesComponent implements OnInit {
  panelOpenState = false;
  isLoggedIn: boolean;
  currentUser: User;
  currentUserEmail = 'akash.kumar@gmail.com';
  giftIds = [];
  usersGift = [];

  constructor(
    public userService: UserService,
    public giftService: GiftService,
    private store: Store<{ authState }>,
    private _snackBar: MatSnackBar,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    this.store.select('authState').subscribe((authState: State) => {
      this.isLoggedIn = authState.isLoggedIn;
      if (this.isLoggedIn) {
        this.currentUser = authState.user;
        this.currentUser.favorites.forEach(giftId => {
          this.giftService.getCardsById(giftId).subscribe(gift => {
            this.usersGift.push(gift);
          });
        });
      } else {
        this.currentUser = authState.user;
      }
    });
  }

  openDialogForGiftNow(currentGift: Gift): void {
    if (!this.isLoggedIn) {
      this.openSnackBar('loginGift');
      return;
    }
    if (this.currentUser.balancePoints < currentGift.buyoutPoints) {
      this.openSnackBar('lessBalance');
      return;
    }
    const dialogRef = this.dialog.open(EditgiftComponent, {
      width: '250px',
      data: {
        gift: currentGift,
        user: this.currentUser,
        message: 'giftnow'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
      // console.log(result);
      if (result) {
        this.email(result);
        this.openSnackBar('sendSuccess');
        // check for success later
        // reduce balance
        this.currentUser.balancePoints =
          this.currentUser.balancePoints - currentGift.buyoutPoints;
        // make api call to update user
        this.userService.updateUser(this.currentUser).subscribe();
      }
    });
  }

  removeFromFavorite(currentGiftId: string) {
    // from the array to display
    for (let i = 0; i < this.usersGift.length; i++) {
      if (this.usersGift[i].id === currentGiftId) {
        this.usersGift.splice(i, 1);
      }
    }
    // required
    console.log(currentGiftId);
    console.log(this.currentUser.favorites);
    // find index and remove from favorite
    const index = this.currentUser.favorites.findIndex(
      giftId => giftId === currentGiftId // find index in array
    );
    this.currentUser.favorites.splice(index, 1); // remove element from array
    // Make a put call to update
    this.userService
      .updateUser(this.currentUser)
      .subscribe(
        user => this.openSnackBar('removeSuccess'),
        error => this.openSnackBar('removeError')
      );
  }

  public email(result): void {
    const templateParams = {
      from_name: 'Team YoYoGift',
      to_name: result.receiverNameControl,
      toEmail: result.receiverEmailControl,
      message_html:
        '<h1>' +
        result.messageControl +
        '</h1><br><a href="https://yoyogifts301.firebaseapp.com/">Redeem</a>'
    };

    Email.send(
      'sendgrid',
      'template_wip4OUkH',
      templateParams,
      'user_qbov9EGUjkJiDJxkuPoQ0'
    ).then(
      function(response) {
        console.log('SUCCESS!', response.status, response.text);
      },
      function(err) {
        console.log('FAILED...', err);
      }
    );
  }

  openSnackBar(message: string) {
    if (message === 'loginFav') {
      this._snackBar.open('Login to Add Favorite !!!', '', {
        duration: 3000
      });
    }
    if (message === 'removeSuccess') {
      this._snackBar.open(
        'Successfully Removed Gift Card from Favorites!!!',
        '',
        {
          duration: 3000
        }
      );
    }
    if (message === 'removeError') {
      this._snackBar.open(
        'Error while Removing Gift Card from Favorites!!! \n Check your internet connection and Try Again!!!',
        '',
        {
          duration: 3000
        }
      );
    }
    if (message === 'loginGift') {
      this._snackBar.open('Login to Send Gift !!!', '', {
        duration: 3000
      });
    }
    if (message === 'lessBalance') {
      this._snackBar.open(
        'Your balance is low ( ' + this.currentUser.balancePoints +  ' ) !!!\n Top Up to send gift Now. !!!',
        '',
        {
          duration: 3000
        }
      );
    }
    if (message === 'sendSuccess') {
      this._snackBar.open('Gift sent successfully!!!', '', {
        duration: 3000
      });
    }
    if (message === 'sendError') {
      this._snackBar.open(
        'Error while Sending Gift Card !!! \n Check your internet connection and Try Again!!!',
        '',
        {
          duration: 3000
        }
      );
    }
  }
}
