import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserSerarchResultsComponent } from './user-serarch-results.component';

describe('UserSerarchResultsComponent', () => {
  let component: UserSerarchResultsComponent;
  let fixture: ComponentFixture<UserSerarchResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserSerarchResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserSerarchResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
