import { Gift } from "src/app/shared/model/gift.model";
import * as UserActions from './user.actions';
const initialState  = {
    gifts: [],
}

export function userStoreReducers(state = initialState, action: UserActions.UserActions){
    switch(action.type){
        case UserActions.addGifts :
            return {
                ...state,
                gifts: [...state.gifts, ...action.payload]
            }
        default :
            return state;
    }
}