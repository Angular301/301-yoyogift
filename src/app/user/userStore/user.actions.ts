import { Action } from '@ngrx/store';

export const addGifts = 'ADD_GIFTS';

export class AddGifts implements Action {
    readonly type = addGifts;
    constructor(public payload){
    }
}

export type UserActions = AddGifts;