import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserGiftDetailsComponent } from './user-gift-details/user-gift-details.component';
import { UserReviewRatingComponent } from './user-review-rating/user-review-rating.component';
import { UserSendGiftComponent } from './user-send-gift/user-send-gift.component';
import { UserCommentPopupComponent } from './user-comment-popup/user-comment-popup.component';
import { UserTopupComponent } from './user-topup/user-topup.component';
import { UserGiftHistoryComponent } from './user-gift-history/user-gift-history.component';
import { UserFavouritesComponent } from './user-favourites/user-favourites.component';
import { MaterialModule } from '../shared/material.module';
import { UserRoutingModule } from './user-routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { UserCategorizedGiftsComponent } from './user-categorized-gifts/user-categorized-gifts.component';
import { CoreModule } from '../core/core.module';
import { UserSerarchResultsComponent } from './user-serarch-results/user-serarch-results.component';
import { SharedModule } from '../shared/shared.module';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { UserRedeemComponent } from './user-redeem/user-redeem.component';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, '../../assets/i18n/', '.json');
}


@NgModule({
  declarations: [
    UserGiftDetailsComponent,
    UserReviewRatingComponent,
    UserSendGiftComponent,
    UserCommentPopupComponent,
    UserTopupComponent,
    UserGiftHistoryComponent,
    UserFavouritesComponent,
    UserFavouritesComponent,
    UserCategorizedGiftsComponent,
    UserSerarchResultsComponent,
    UserRedeemComponent
  ],
  imports: [CommonModule, MaterialModule, UserRoutingModule, FlexLayoutModule, CoreModule, SharedModule, ScrollingModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ]
})
export class UserModule { }
