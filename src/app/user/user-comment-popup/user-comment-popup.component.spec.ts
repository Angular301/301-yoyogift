import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserCommentPopupComponent } from './user-comment-popup.component';

describe('UserCommentPopupComponent', () => {
  let component: UserCommentPopupComponent;
  let fixture: ComponentFixture<UserCommentPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserCommentPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserCommentPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
