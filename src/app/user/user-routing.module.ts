import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserGiftDetailsComponent } from './user-gift-details/user-gift-details.component';
import { Routes, RouterModule } from '@angular/router';
import { UserCategorizedGiftsComponent } from './user-categorized-gifts/user-categorized-gifts.component';
import { UserFavouritesComponent } from './user-favourites/user-favourites.component';
import { AuthGuardService } from '../core/auth/auth-guard.service';
import { UserRedeemComponent } from './user-redeem/user-redeem.component';

const routes: Routes = [
  {
    path: '',
    component: UserCategorizedGiftsComponent
  },
  {
    path: 'details/:id',
    component: UserGiftDetailsComponent
  },
  {
    path: 'favorite',
    component: UserFavouritesComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'redeem',
    component: UserRedeemComponent
  }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)]
})
export class UserRoutingModule { }
