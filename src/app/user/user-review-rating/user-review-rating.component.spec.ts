import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserReviewRatingComponent } from './user-review-rating.component';

describe('UserReviewRatingComponent', () => {
  let component: UserReviewRatingComponent;
  let fixture: ComponentFixture<UserReviewRatingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserReviewRatingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserReviewRatingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
