import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GiftService } from 'src/app/core/service/gift.service';
import { Gift } from 'src/app/shared/model/gift.model';
import { UserService } from 'src/app/core/service/user.service';
import { User } from 'src/app/shared/model/user.model';
import { MatDialog } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { State } from 'src/app/core/auth/authstore/state.model';
import { EditgiftComponent } from 'src/app/shared/components/editgift/editgift.component';
import * as Email from 'emailjs-com';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserGift } from 'src/app/shared/model/user-gift.class';

@Component({
  selector: 'app-user-gift-details',
  templateUrl: './user-gift-details.component.html',
  styleUrls: ['./user-gift-details.component.scss']
})
export class UserGiftDetailsComponent implements OnInit {
  currentGift: Gift;
  giftId: string;
  isLoading = true;
  isLoggedIn: boolean;
  reviews = [];
  currentUser: User;
  durationInSeconds = 5;
  userGift: UserGift;

  constructor(
    public giftService: GiftService,
    public route: ActivatedRoute,
    public userService: UserService,
    public dialog: MatDialog,
    private store: Store<{ authState }>,
    public router: Router,
    private _snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.isLoading = true;
    this.route.paramMap.subscribe(params => {
      this.giftId = params.get('id');
    });
    if (this.giftId) {
      this.getGiftById(this.giftId);
      this.getReviews(this.giftId);
    }
    this.store.select('authState').subscribe((authState: State) => {
      this.isLoggedIn = authState.isLoggedIn;
      if (this.isLoggedIn) {
        this.currentUser = authState.user;
      } else {
        this.currentUser = authState.user;
      }
    });
  }

  getGiftById(giftId: string): void {
    this.giftService.getCardsById(giftId).subscribe(
      gift => {
        if (gift) {
          this.currentGift = gift;
        }
        this.isLoading = false;
      },
      error => {
        this.isLoading = false;
      }
    );
  }

  getReviews(giftId: string): void {
    this.isLoading = true;
    this.giftService.getGiftReviews(giftId).subscribe(reviews => {
      reviews.forEach(review => {
        this.reviews.push(review);
        this.isLoading = false;
      });
    });
  }

  addToFavorite(currentGiftId: string): void {
    if (!this.isLoggedIn) {
      this.openSnackBar('loginFav');
      return;
    }
    let alreadyPresent = false;
    console.log(this.currentUser);
    console.log(this.currentUser.favorites);
    alreadyPresent = this.currentUser.favorites.some(
      giftId => giftId === currentGiftId
    );
    console.log(alreadyPresent);
    if (alreadyPresent) {
      this.openSnackBar('alreadyPresent');
      return;
    }
    console.log('Pushing');
    this.currentUser.favorites.push(currentGiftId);
    console.log('new favorate:' + this.currentUser.favorites);
    console.log('Calling Update');
    this.userService.updateUser(this.currentUser).subscribe();
    this.openSnackBar('addSuccess');
  }

  openDialogForGiftNow(): void {
    if (!this.isLoggedIn) {
      this.openSnackBar('loginGift');
      return;
    }
    if (this.currentUser.balancePoints < this.currentGift.buyoutPoints) {
      this.openSnackBar('lessBalance');
      return;
    }
    const dialogRef = this.dialog.open(EditgiftComponent, {
      width: '250px',
      data: {
        gift: this.currentGift,
        user: this.currentUser,
        message: 'giftnow'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
      // console.log(result);
      if (result) {
        this.email(result);
        this.openSnackBar('sendSuccess');
        // reduce balance
        this.currentUser.balancePoints =
          this.currentUser.balancePoints - this.currentGift.buyoutPoints;
        // make api call to update user
        this.userService.updateUser(this.currentUser).subscribe();
        // add to user-gift
        this.userGift = new UserGift(
          +this.currentUser.id,
          this.currentUser.email,
          result.receiverEmailControl,
          this.currentGift.id,
          this.currentGift.name
        );
        this.giftService.addUserGift(this.userGift).subscribe();
      }
    });
  }

  public email(result): void {
    const templateParams = {
      from_name: 'Team YoYoGift',
      to_name: result.receiverNameControl,
      toEmail: result.receiverEmailControl,
      message_html:
        '<h1>' +
        result.messageControl +
        '</h1><br><a href="https://yoyogifts301.firebaseapp.com/gifts/redeem">Redeem</a>'
    };

    Email.send(
      'sendgrid',
      'template_wip4OUkH',
      templateParams,
      'user_qbov9EGUjkJiDJxkuPoQ0'
    ).then(
      function(response) {
        console.log('SUCCESS!', response.status, response.text);
      },
      function(err) {
        console.log('FAILED...', err);
        this.openSnackBar('sendError');
      }
    );
  }

  openSnackBar(message: string) {
    if (message === 'loginFav') {
      this._snackBar.open('Login to Add Favorite !!!', '', {
        duration: 3000
      });
    }
    if (message === 'alreadyPresent') {
      this._snackBar.open('This gift is already in your Favorites !!!', '', {
        duration: 3000
      });
    }
    if (message === 'addSuccess') {
      this._snackBar.open('Successfully Added to Favorites !!!', '', {
        duration: 3000
      });
    }
    if (message === 'addError') {
      this._snackBar.open(
        'Error while Adding to Favorites !!! \n Check your internet connection and Try Again !!!',
        '',
        {
          duration: 3000
        }
      );
    }
    if (message === 'loginGift') {
      this._snackBar.open('Login to Send Gift !!!', '', {
        duration: 3000
      });
    }
    if (message === 'lessBalance') {
      this._snackBar.open(
        'Your balance is low (' + this.currentUser.balancePoints + ')!!!\n Top Up to send gift Now. !!!',
        '',
        {
          duration: 3000
        }
      );
    }
    if (message === 'sendSuccess') {
      this._snackBar.open('Gift sent successfully !!!', '', {
        duration: 3000
      });
    }
    if (message === 'sendError') {
      this._snackBar.open(
        'Error while Sending Gift Card !!! \n Check your internet connection and Try Again !!!',
        '',
        {
          duration: 3000
        }
      );
    }
  }
}
