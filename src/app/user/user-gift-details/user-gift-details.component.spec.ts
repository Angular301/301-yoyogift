import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserGiftDetailsComponent } from './user-gift-details.component';
import { LoaderComponent } from 'src/app/core/components/loader/loader.component';
import { MaterialModule } from 'src/app/shared/material.module';
import { NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { UserService } from 'src/app/core/service/user.service';
import { Observable, Subject, of, BehaviorSubject } from 'rxjs';
import { GiftService } from 'src/app/core/service/gift.service';
import { Store } from '@ngrx/store';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

class MockService {
  public paramMap = new Observable;

  public getCardsById(): any {
    return of({
      id: '1937374548',
      name: 'Olive - Spread Tapenade',
      brand: 'thomcook',
      desc: 'Carrier of infections with a predominantly sexual mode of transmission',
      imageUrl: 'http://dummyimage.com/500x334.png/dddddd/000000&text=Olive - Spread Tapenade',
      buyoutPoints: 503386,
      categoryId: 7,
      rating: 4,
      discount: 12
    });
  }
  public select(mapFn: any, ...paths: string[]): Observable<any> {
    const val = {
      globalState: {
        transactionInitState: [{}],
        accountAmountState: [{}]
      }
    };
    return of([val]);
  }
  public dispatch(): void {
    return null;
  }
  public navigate(): any { }

  public getGiftReviews(): any {
    return of([]);
  }
  public open(): any { }

  public get(): any{

  }
}

describe('UserGiftDetailsComponent', () => {
  let component: UserGiftDetailsComponent;
  let fixture: ComponentFixture<UserGiftDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [],
      declarations: [UserGiftDetailsComponent],
      schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        { provide: ActivatedRoute, useClass: MockService },
        { provide: UserService, useClass: MockService },
        { provide: GiftService, useClass: MockService },
        { provide: Store, useClass: MockService },
        { provide: Router, useClass: MockService },
        { provide: MatDialog, useClass: MockService },
        { provide: MatSnackBar, useClass: MockService }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserGiftDetailsComponent);
    component = fixture.componentInstance;
    component.currentGift = {
      id: '1937374548',
      name: 'Olive - Spread Tapenade',
      brand: 'thomcook',
      desc: 'Carrier of infections with a predominantly sexual mode of transmission',
      imageUrl: 'http://dummyimage.com/500x334.png/dddddd/000000&text=Olive - Spread Tapenade',
      buyoutPoints: 503386,
      expiryDays: 78,
      categoryId: 7,
      rating: 4,
      discount: 12
    };
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call ngOnInit', () => {
    component.ngOnInit();
  });
  it('should call getGiftById', () => {
    component.getGiftById('1');
  });
  it('should call getReviews', () => {
    component.getReviews('1');
  });
  it('should call addToFavorite', () => {
    component.addToFavorite('1');
  });
  it('should call openDialogForGiftNow', () => {
    component.openDialogForGiftNow();
  });
  it('should call openDialogForGiftNow', () => {
    component.openSnackBar('loginFav');
  });
  it('should call openDialogForGiftNow', () => {
    component.openSnackBar('removeSuccess');
  });
  it('should call openDialogForGiftNow', () => {
    component.openSnackBar('removeError');
  });
  it('should call openDialogForGiftNow', () => {
    component.openSnackBar('loginGift');
  });
});
