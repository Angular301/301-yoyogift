import { Component, OnInit, Input } from '@angular/core';
import { MatDialog, MatDialogRef, MatDialogConfig, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DeleteGiftComponent } from '../delete-gift/delete-gift.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { CardService } from '../services/card.service';
import { Store } from '@ngrx/store';
import { State } from 'src/app/core/auth/authstore/state.model';
@Component({
  selector: 'app-gift-card',
  templateUrl: './gift-card.component.html',
  styleUrls: ['./gift-card.component.scss']
})
export class GiftCardComponent implements OnInit {
  @Input() data;
  giftData: any;
  public loggedIn = false;
  public error = false;
  public name: any;
  public isAdmin: any;
  public img: any;

  simpleDialog: MatDialogRef<DeleteGiftComponent>;

  constructor(private dialogModel: MatDialog,
    public cardService: CardService,
    public router: Router,
    private store: Store<{ authState: State }>) { }

  ngOnInit() {
    this.store.select('authState').subscribe(data => {
      if (data.user !== null) {
        this.isAdmin = data.user.isAdmin;
      }
    });
  }

  dialog() {
    this.giftData = this.data;
    this.simpleDialog = this.dialogModel.open(DeleteGiftComponent, {
      width: '600px',
      data: {
        gift: this.giftData,
        message: 'deletecard'
      }
    });
  }

  openGiftDetails(giftId: string): void {
    this.router.navigate(['/gifts/details/', giftId]);
  }

  editgift() {
    this.giftData = this.data;
    this.simpleDialog = this.dialogModel.open(DeleteGiftComponent, {
      width: '50%',
      height: '80%',
      data: { gift: this.giftData, message: 'editcard' }
    });

    this.simpleDialog.afterClosed().subscribe(result => {
      if (result) {
        this.giftData = result;
        this.giftData.data.id = this.data.id;
        this.data = this.giftData.data;
      }
    });
  }

}
