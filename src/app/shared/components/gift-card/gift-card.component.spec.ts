import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { GiftCardComponent } from './gift-card.component';
import { NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CardService } from '../services/card.service';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/internal/Observable';
import { of } from 'rxjs/internal/observable/of';

class MockService {
  public select(): Observable<any> {
    const data = {
      id: 1
    };
    return of([data]);
  }
  public dispatch(): void {
    return null;
  }
}
describe('GiftCardComponent', () => {
  let component: GiftCardComponent;
  let fixture: ComponentFixture<GiftCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GiftCardComponent],
      schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        { provide: MatDialog, useClass: MockService },
        { provide: CardService, useClass: MockService },
        { provide: Router, useClass: MockService },
        { provide: Store, useClass: MockService }
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GiftCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    component.data = {
      id: '1937374548',
      name: 'Olive - Spread Tapenade',
      brand: 'thomcook',
      desc: 'Carrier of infections with a predominantly sexual mode of transmission',
      imageUrl: 'http://dummyimage.com/500x334.png/dddddd/000000&text=Olive - Spread Tapenade',
      buyoutPoints: 503386,
      expiryDays: 78,
      categoryId: 7,
      rating: 4,
      discount: 12
    };
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
