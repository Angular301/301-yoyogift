import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CardService } from '../services/card.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { HttpServiceService } from 'src/app/core/service/http-service.service';

@Component({
  selector: 'app-delete-gift',
  templateUrl: './delete-gift.component.html',
  styleUrls: ['./delete-gift.component.scss']
})
export class DeleteGiftComponent implements OnInit {
  durationInSeconds: 5;
  giftcard: any;
  EditForm: FormGroup;
  category: any;
  categoryname: string;
  categorylength: 0;

  constructor(
    private _snackBar: MatSnackBar,
    public cardService: CardService,
    public httpService: HttpServiceService,
    public dialogRef: MatDialogRef<DeleteGiftComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  public ngOnInit() {

    this.setcategory();

    this.EditForm = new FormGroup({

      name: new FormControl(this.data.gift.name, Validators.required),
      brand: new FormControl(this.data.gift.brand, Validators.required),
      desc: new FormControl(this.data.gift.desc),
      imageUrl: new FormControl(this.data.gift.imageUrl, Validators.required),
      buyoutPoints: new FormControl(this.data.gift.buyoutPoints,
        [Validators.required, Validators.maxLength(5), Validators.pattern(/^.?(0|[1-9]\d*)?$/)]),
      categoryId: new FormControl(this.data.gift.categoryId, Validators.required),
      rating: new FormControl(this.data.gift.rating,
        [Validators.required, Validators.maxLength(1), Validators.pattern(/^.?(0|[1-5]\d*)?$/)]),
      discounts: new FormControl(this.data.gift.discounts,
        [Validators.required, Validators.maxLength(2), Validators.pattern(/^%?(0|[1-9]\d*)?$/)])
    });

    if (this.data.message === 'deletecard') {
      this.dialogRef.updateSize('300px', '200px');
    }
    if (this.data.message === 'editcard') {
      this.dialogRef.updateSize('50%', '70%');
    }

  }

  setcategory() {
    this.httpService.get('/category').subscribe(
      response => {
        this.category = response;
        this.categorylength = this.category.length;
        for (let i = 0; i < this.categorylength; i++) {
          if (+this.data.gift.categoryId === +this.category[i].id) {
            this.categoryname = this.category[i].name;
          }
        }
      });
  }

  public close(): void {
    this.dialogRef.close();
  }

  public delete(id: string) {
    this.cardService.deleteCard(id).subscribe(
      msg => this.openSnackBardelete('deleted successfully!', id),
      error => this.openSnackBardelete('could not delete!', id)
    );

    this.dialogRef.close();
  }

  openSnackBardelete(inputval: string, id: string) {
    if (inputval === 'deleted successfully!') {
      document.getElementById(id).style.display = 'none';
    }
    const snackBaridelete = this._snackBar.open(inputval, '', {
      duration: 3000
    });
  }

  editgift() {
    this.cardService.updateCard(this.data.gift.id, this.EditForm.value)
      .subscribe(
        Gift => this.openSnackBaredit('Saved successfully!', Gift),
        (error) => this.openSnackBaredit('could not save', '')
      );
  }

  openSnackBaredit(inputval, gift) {
    const snackBaredit = this._snackBar.open(inputval, '', {
      duration: 3000
    });
    if (inputval === 'Saved successfully!') {
      this.data.gift = gift;
      this.dialogRef.close({ data: this.data.gift });
    }
  }

}
