import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DeleteGiftComponent } from './delete-gift.component';
import { NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { CardService } from '../services/card.service';
import { of } from 'rxjs/internal/observable/of';
import { throwError } from 'rxjs/internal/observable/throwError';
import { FormGroup } from '@angular/forms';

class MockService {
  public data = {
    gift: {
      name: 'name'
    },
    id: '1'
  };
  public close(): any { }

  public deleteCard(): any {
    return of([]);
  }

  public get(): any {
    return of([
      {
        name: 'name'
      }
    ]);
  }
  public open(): any { }

  public updateCard(): any {
    return of([]);
  }
}

describe('DeleteGiftComponent', () => {
  let component: DeleteGiftComponent;
  let fixture: ComponentFixture<DeleteGiftComponent>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [],
      declarations: [DeleteGiftComponent],
      providers: [
        { provide: HttpClient, useClass: MockService },
        { provide: MatDialogRef, useClass: MockService },
        { provide: MAT_DIALOG_DATA, useClass: MockService },
        { provide: CardService, useClass: MockService },
        { provide: MatSnackBar, useClass: MockService }
      ],
      schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteGiftComponent);
    component = fixture.componentInstance;
    component.data = {
      gift: {
        name: 'name'
      },
      id: '1'
    };
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should call ngOninit', () => {
    component.ngOnInit();
  });

  it('should call close', () => {
    component.close();
  });
  it('should call editgift', () => {
    component.EditForm = new FormGroup({});
    component.editgift();
  });

  it('should call delete', () => {
    const cardService = TestBed.get(CardService);
    spyOn(cardService, 'deleteCard').and.returnValue(
      throwError([])
    );
    component.delete(component.data.id);
  });
});
