import { TestBed } from '@angular/core/testing';

import { CardService } from './card.service';
import { HttpClient } from '@angular/common/http';

class MockService {
  public get(): any { }

  public delete(): any { }

}

describe('CardService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      { provide: HttpClient, MockService }
    ]
  }));

  it('should be created', () => {
    const service: CardService = TestBed.get(CardService);
    expect(service).toBeTruthy();
  });
});
