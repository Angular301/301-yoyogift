import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Gift } from 'src/app/shared/model/gift.model';
import { Observable } from 'rxjs/internal/Observable';

const JSON_REPO_URL_GIFT = 'https://json-repo.herokuapp.com/gifts/';

@Injectable({
  providedIn: 'root'
})
export class CardService {
  constructor(public http: HttpClient) { }

  getAllCards(): Observable<Gift[]> {
    return this.http.get<Gift[]>(JSON_REPO_URL_GIFT);
  }

  getCardsById(id: string): Observable<Gift> {
    return this.http.get<Gift>(JSON_REPO_URL_GIFT + id);
  }

  updateCard(id: string, newCard: Gift): Observable<Gift> {
    return this.http.put<Gift>(JSON_REPO_URL_GIFT + id, newCard);
  }

  deleteCard(id: string): Observable<Gift> {
    return this.http.delete<Gift>(JSON_REPO_URL_GIFT + id);
  }
}