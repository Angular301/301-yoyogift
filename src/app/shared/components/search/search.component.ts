import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpServiceService } from 'src/app/core/service/http-service.service';
import { debounceTime } from 'rxjs/internal/operators/debounceTime';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  public SearchControl = new FormControl();
  public options = [];
  public searchData: string;

  constructor(
    public router: Router,
    public httpService: HttpServiceService
  ) { }

  ngOnInit() {
    this.SearchControl.valueChanges.pipe(
      debounceTime(1000)
    ).subscribe(value => {
      if (value === '') {
        this.options = [];
      } else {
        this.httpService.get(`/gifts?q=${value}&_limit=5`).subscribe((data: any) => {
          this.options = data;
        });
      }
    });
  }

  public routeTosearchResult(event: any): void {
    this.router.navigate(['/gifts'], { queryParams: { source: 'search', query: event.target.value } });
  }
}
