import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { EditgiftComponent } from './editgift.component';
import { MaterialModule } from '../../material.module';
import { CardService } from '../services/card.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { of } from 'rxjs/internal/observable/of';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

class MockService {
  public updateCard(): any {
    return of([]);
  }

  public getCardsById(): any {
    return of([]);
  }

  public close(): void { }
}

describe('EditgiftComponent', () => {
  let component: EditgiftComponent;
  let fixture: ComponentFixture<EditgiftComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MaterialModule, NoopAnimationsModule],
      declarations: [EditgiftComponent],
      providers: [
        { provide: CardService, useClass: MockService },
        { provide: MatDialogRef, useClass: MockService },
        { provide: MAT_DIALOG_DATA, useClass: MockService }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditgiftComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call close', () => {
    component.close();
  });

  // it('should call edit', () => {
  //   component.edit(1);
  // });
});
