import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CardService } from '../services/card.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-editgift',
  templateUrl: './editgift.component.html',
  styleUrls: ['./editgift.component.scss']
})
export class EditgiftComponent implements OnInit {
  // For Send Gift
  receiverForm: FormGroup;
  // For Rate and review
  reviewForm: FormGroup;
  rating: number;
  oldReview: string;
  constructor(
    public cardService: CardService,
    public dialogRef: MatDialogRef<EditgiftComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.oldReview = this.data.review;
  }

  ngOnInit() {
    if (this.data.message === 'viewrate') {
      this.rating = this.data.rating;
    } else {
      this.rating = 4;
    }
    if(this.data.message === 'giftnow') {
      this.dialogRef.updateSize('65%', '55%');
    }
    if(this.data.message === 'ratenow') {
      this.dialogRef.updateSize('55%', '40%');
    }
    if(this.data.message === 'viewrate') {
      this.dialogRef.updateSize('55%', '40%');
    }
    this.receiverForm = new FormGroup({
      receiverNameControl: new FormControl('', [Validators.required]),
      receiverEmailControl: new FormControl('', [
        Validators.required,
        Validators.email,
        Validators.pattern(
          /^[^-s]+[a-zA-Z0-9_]{1}[a-zA-Z0-9._]*@[a-zA-Z0-9]+.[a-zA-Z]{1,2}.?[a-zA-Z]{2}$/
        )
      ]),
      messageControl: new FormControl('', [Validators.required])
    });
    this.reviewForm = new FormGroup({reviewControl: new FormControl('', [Validators.required])});
  }

  onClick(newRating: number) {
    this.rating = newRating;
  }

  public close(): void {
    this.dialogRef.close();
  }
  sendEmail() {
    console.log(this.receiverForm.value);
    this.close();
  }
  get receiverName() {
    return this.receiverForm.get('receiverNameControl');
  }
  get receiverEmail() {
    return this.receiverForm.get('receiverEmailControl');
  }
  get message() {
    return this.receiverForm.get('messageControl');
  }
  get review() {
    return this.reviewForm.get('reviewControl');
  }
  getReceiverNameError(): string {
    if (this.receiverName.hasError('required')) {
      return 'Receiver Name Required';
    } else {
      return '';
    }
  }
  getReceiverEmailError(): string {
    if (this.receiverEmail.hasError('required')) {
      return 'Email Required';
    } else if (this.receiverEmail.hasError('pattern')) {
      return 'Invalid email';
    } else if (this.receiverEmail.hasError('email')) {
      return 'Invalid email';
    } else {
      return '';
    }
  }
  getMessageError(): string {
    if (this.message.hasError('required')) {
      return 'Message Required';
    } else {
      return '';
    }
  }
  getReviewError(): string {
    if (this.review.hasError('required')) {
      return 'Please Provide your Review!!!';
    } else {
      return '';
    }
  }
}
