export class User {
  constructor(
    public name: string,
    public email: string,
    public balancePoints: number,
    public isAdmin: boolean,
    public favorites: string[],
    public image: string,
    public id?: string
  ) {
    this.id = id;
    this.name = name;
    this.email = email;
    this.image = image;
    this.favorites = favorites;
    this.balancePoints = balancePoints;
  }
}
