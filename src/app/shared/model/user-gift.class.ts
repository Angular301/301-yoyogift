export class UserGift {
  constructor(
    public senderId: number,
    public senderEmail: string,
    public receiverEmail: string,
    public giftId: string,
    public giftName: string,
    public id?: number
  ) {
    this.id = id;
    this.senderId = senderId;
    this.senderEmail = senderEmail;
    this.receiverEmail = receiverEmail;
    this.giftId = giftId;
    this.giftName = giftName;
  }
}
