export class Review {
  constructor(
    public userId: number,
    public userName: string,
    public giftId: string,
    public rating: number,
    public review: string,
    public reviewedAt: Date,
    public id?: number
  ) {
    this.id = id;
    this.userId = userId;
    this.userName = userName;
    this.giftId = giftId;
    this.rating = rating;
    this.review = review;
    this.reviewedAt = reviewedAt;
  }
}
