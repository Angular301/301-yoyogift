import { Gift } from './gift.model';

describe('Gift', () => {
  beforeEach(() => {
    const gift: Gift = {
      name: '',
      brand: '',
      desc: '',
      imageUrl: '',
      buyoutPoints: 0,
      expiryDays: 0,
      categoryId: 0,
      rating: 0,
      id: '',
      discount: 0
    };
  });

  it('should create an instance', () => {
    expect(new Gift('', '', '', '', 0, 0, 0, 0, 0)).toBeTruthy();
  });
});
