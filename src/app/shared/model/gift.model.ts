export class Gift {
  constructor(
    public name: string,
    public brand: string,
    public desc: string,
    public imageUrl: string,
    public buyoutPoints: number,
    public expiryDays: number,
    public categoryId: number,
    public rating: number,
    public discount: number,
    public id?: string
  ) {
    this.id = id;
    this.name = name;
    this.brand = brand;
    this.desc = desc;
    this.imageUrl = imageUrl;
    this.buyoutPoints = buyoutPoints;
    this.expiryDays = expiryDays;
    this.categoryId = categoryId;
    this.rating = rating;
    this.discount = discount;
  }
}

export interface GiftState {
  name: string;
  brand: string;
  desc: string;
  imageUrl: string;
  buyoutPoints: number;
  expiryDays: number;
  categoryId: number;
  rating: number;
  id: string;
}
