import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchComponent } from './components/search/search.component';
import { GiftCardComponent } from './components/gift-card/gift-card.component';
import { ErrorComponent } from './components/error/error.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '../shared/material.module';
import { DeleteGiftComponent } from './components/delete-gift/delete-gift.component';
import { EditgiftComponent } from './components/editgift/editgift.component';

@NgModule({
  declarations: [
    SearchComponent,
    GiftCardComponent,
    ErrorComponent,
    DeleteGiftComponent,
    EditgiftComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MaterialModule
  ],
  exports: [
    SearchComponent,
    GiftCardComponent,
    ErrorComponent,
    DeleteGiftComponent,
    EditgiftComponent
  ],

  entryComponents: [DeleteGiftComponent, EditgiftComponent]


})
export class SharedModule { }
